package com.example.UserRepo.Enum;

public enum Role {
    SUPER_ADMIN,
    ADMIN,
    TRAINER,
    TRAINEE
}

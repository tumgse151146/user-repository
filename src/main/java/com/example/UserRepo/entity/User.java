package com.example.UserRepo.entity;

import com.example.UserRepo.Enum.Gender;
import com.example.UserRepo.Enum.Role;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String avata;
    private String email;
    private String phone;
    private Date dob;
    private Gender gender;
    private String createdBy;
    private Date createdAt;
    private Role role;
    private String status;

}
